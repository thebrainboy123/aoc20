import unittest
import AOC2020


class testAOC2020(unittest.TestCase):

    def test_AOCD1P1_sample(self):
        result = AOC2020.AOCD1P1([1721, 979, 366, 299, 675, 1456])
        self.assertEqual(result, 514579)

