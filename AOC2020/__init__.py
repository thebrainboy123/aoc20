def build_list(file):
    data_file_content = open(file, "r")
    data_file_content = data_file_content.read().splitlines()
    dataset =[]
    for data in data_file_content:
        dataset.append(int(data))

    return dataset

def AOCD1P1(list):
    counter = 1
    for x in list:
        for y in list[counter:len(list)]:
            if x + y == 2020:
                return x*y
        counter += 1
    return "fail"
def main():
    dataset = build_list("AOCD1P1_data.txt")
    result = AOCD1P1(dataset)
    print(result)
main()